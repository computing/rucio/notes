# IGWN rucio guide

This site attempts to describe the following

- The Rucio deployment for administrators / operators.
- Data registration and management for IGWN operators and users.
- End-point configurations for site administrators.
- Data discovery and access for IGWN users.

This site is based on the [IGWN Computing
Guide](https://computing.docs.ligo.org/) whose authors we gratefully
acknowledge for providing template documentation.

## To-do

- [x]  Export production db to file
- [x]  Configure dev env from upstream `docker-compose` recipe
- [x]  Stand up copy of production server in dev environment
- [x]  Import production db into dev environment
- [x]  Stand up experimental dev server
- [x]  Set up accounts in dev server
- [x]  Develop application to export, manipulate, import production db state
- [ ]  Determine new namespace organisation
- [ ]  Determine best practices for registration, replication in new namespace
- [ ]  Construct LFN2PFN algorithm to handle our intended usage
- [ ]  Application/infrastructure to generate RSEs
- [ ]  Set up dummy RSE network in dev server instance
- [ ]  Reproduce state of production db / replicas in dev instance
- [ ]  Deploy live dev instance
- [ ]  Test registration & replication pipeline

### Current focus / roadmap

- [ ] Configure RSE network in `igwn-dev` environment
- [ ] Devise new LFN2PFN algorithm(s)
