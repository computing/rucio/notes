# A new naming scheme

## Goals

1. Establish a long(er)-term sustainable namespace (scope, accounts)
   architecture for a new rucio deployment (topology, configuration)
1. Demonstrate how to import the state (DID metadata, replica information) of
   the current IGWN deployment into a new rucio deployment.

## "Old" topology

The current rucio instance uses:

1. Scopes = observing runs (ER8, O1, O2, etc)
1. All rules managed by the root account

This works ok as a drop-in replacement for the data *replication* aspects of
LDR: in this schema many (but certainly not all) frame paths are easily
determined using just the scope and LFN; data sets tend to be requested by
sites at the observing run level and there has been only a single operator and
no need for access control.

It is not, however, extensible to the user- or pipeline-level: we do not, for
example, want to give pycbc (say) the same degree of access to rule management
as a site administrator whose users want a certain frame data set.   It is also
an impractical way to manage data discovery: native rucio queries are defined
by their scope; the current scheme complicates finding (say) frame files which
span the ER8 and O1 boundaries.

In addition, we have ended up setting up multiple RSEs at the same storage
locations, just to handle different naming conventions.  This is as much a bad
lfn2pfn algorithm as anything but it's also something to be mindful of moving
forward.

## New topology

The basic idea is to:

1. support a wide range of data types and users / groups on top of
   centrally-managed frames and, to some extent, SFTs
1. Allow realistic use-cases for data discovery.

Start with frames and SFT, as before.  Here's an existing datafind query:

    python -m gwdatafind --observatory H --type H1_HOFT_C02 \
      -s 1126259460 -e $((1126259462+2)) --server=datafind.ligo.org:443

Some notes:

* All of this metadata can (in principle) be handled by native rucio metadata
  support (either DID- or JSON-level).  
* I'm never likely (?) make a single query for multiple data products (frames,
  SFTs, ...)

The [description from the rucio documentation](https://rucio.readthedocs.io/en/latest/overview_File_Dataset_Container.html):

> The scope string partitions the namespace into several sub namespaces. The
> primary use case for this is to easily separate centrally created data from
> individual user data.
>
> By default, accounts have read access to all scopes and write access only to
> their own scope. Privileged accounts have write access to multiple scopes,
> e.g., the Workload Management System is allowed to write into official
> data scopes.

**Aside**: The [rucio
docs](https://rucio.readthedocs.io/en/latest/replica_workflow.html) talk about
pluggable *non*-deterministic lfn2pfn algorithms based on metadata. We should
explore this more.

### Data products

[Here](https://docs.google.com/spreadsheets/d/12jYEWcRKgCRlzImKpNknopH-dUZGQtTIUl6udoEhenk/edit#gid=0)
is a useful inventory of O3a data products, which includes:

* posterior samples (various groups: population studies, TGR, CBC parameters)
* glitch models (presumably waveforms)
* waveform reconstructions
* sensitivity curves
* cbc search triggers (detections)
* cbc search triggers (entire datasets per detector)

### Scopes and accounts

An obvious solution is to set `scope=data product` so that we use metadata for
discovery and multiple accounts can write to the same scope.  A downside would
be potentially large numbers of (frame, SFT) files in the same scope.

Similarly, let's separate scopes from the root account, in contrast to
previously.  The archetypal use case is still going to be frames & sfts, which
we want in separate scopes, so let's start with that.

Again, from the [rucio
docs](https://rucio.readthedocs.io/en/latest/overview_Rucio_account.html):

> A Rucio account is the unit of assigning privileges in Rucio. It can
> represent individual users (such as user1, user2, user3, ...), a group of
> users (such as group1, group2, group3, ...) or a centralized production
> activity such as service accounts for data generation (datagen) or the
> workflow management systems (wfms).

There are 3 [types of
account](https://rucio.readthedocs.io/en/latest/man/rucio-admin.html#add):

1. USER
1. GROUP
1. SERVICE

!!! example "Possible account and scope setup"

    * account: `bulkdata`
    * scopes: `frames`, `sfts`
    * type: `SERVICE`

### Frame files, datasets and containers

Frame data sets in IGWN are defined by the science run and frame type (like
`H1_HOFT_C00`). The current production instance of rucio defines datasets
with the scope and the frame type, like `O1:H1_HOFT_C00`.  This allows us to
set replication rules with the appropriate level of granularity (e.g. all
C02-level strain data from LHO in O3).

Exploring some options to achieve the same thing in the new setup (using
`H1_HOFT_C02` to illustrate):

1. Include the observing run explicitly in the dataset name: `frames:O3.H1_HOFT_C02`
1. Generate arbitrarily named datasets: use metadata for identification and
   subscriptions to set rules
1. Create a container `O1` which contains datasets `H1_HOFT_C02`,
   `L1_HOFT_C02`, ...
1. Create a container `H1_HOFT_C02` which contains datasets `O1`, `O2`, ...

The second will be a headache to manage.

The last two do not seem feasible as there is no way to manage datasets at the
level of `<observing run>-<datatype>`.

Let's go with the most straightforward option of
`frames:<observing_run>.<type>` for frames with an equivalent configuration for
SFTs.

!!! warning "refinements to this description"

    This is an exploratory note.  See later in this work for how new schemes
    are actually implemented.

### RSEs

Start with three RSEs for demonstration and scale up as needed:

* `LIGO-CIT` (`/archive/frames`): set things up so this RSE can host frames
  *and* sfts.
* `OZSTAR`
* `LIGO-WA` (`/archive/frames`)

This should be sufficient for showing *how* to reproduce replica state.

## Summary

The new schema will start with:

* 2 scopes: `frames`, `sfts`
* 2 `SERVICE` accounts: `root`, `bulkdata` (each with some number of
  identities)
* 3 RSEs, including at least 1 archival location with *existing* datasets

Once we've shown how to reproduce the existing state at these locations and
some number of datasets, we can build out to reproduce the entire current set
of files under rucio management.

From there, we can restore the database in a new production-ready system and
transition to the new framework.
