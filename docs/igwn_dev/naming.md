# DID names & LFN2PFN algorithms

## Scopes

Suggest the following convention for scopes: `<Project>.<account>` with
examples like:

* `LIGO.frames`
* `Virgo.frames`
* `LIGO.sfts`
* `Virgo.sfts`
* `CBC.posteriors`
* `CBC.pycbc`
* `BayesWave.Waveforms`

So then, how do we construct the LFN2PFN algorithm?

Note that in this setup, a deterministic algorithm based on frame file
basenames will require a lookup table to get the observing run.

## LFN2PFN: the problem

The problem: we need to map names like:

    <!-- markdownlint-disable MD013 -->
    LIGO.frames:H-H1_HOFT_C02-1126256640-4096.gwf -> /archive/frames/ER8/hoft_C02/H1/H-H1_HOFT_C02-11262/H-H1_HOFT_C02-1126256640-4096.gwf

Where:

* there is a non-trivial connection between the basename and the directory
  names (e.g. `HOFT_C00` lives in `hoft` but `HOFT_C02` lives in `hoft_c02`).
* For some files (SFTs), there is insufficient information in the basename to
  determine the path.

Criteria:

1. It should be possible to determine multple PFNs from a single LFN.
1. The LFN should be sufficient: we can only use metadata *after* a file has
   been registered.

The following sections discuss some possible configurations.  We keep the most
promising at the top.

### Mix: Register files at non-deterministic RSEs, determine LFNs from basename AND PFNs

The "all of the below" option:

1. Register files at non-deterministic "staging" RSEs (including LIGO-CIT, -WA and -LA).
1. For frames: LFN is `scope:basename`
1. For legacy SFTs: LFN is `scope:pfn.with.substitutions` (e.g.
   `ER8/Hann/C02/some_sft.sft -> ER8.Hann.C02.some_sft.sft`)
1. New SFTs can be supported similarly to frames.
1. Keep non-deterministic and deterministic RSEs at the same location synchronised.

Pros:

* Simple to register files.
* Option to fall back to PFN-lookup while we build-out a fully-deterministic algorithm.
* PFN-based SFT names are quick to convert.

Cons:

* Database growth from non-deterministic RSEs.
* Extra instrumentation required to keep non-deterministic / deterministic
  counterparts synchronised.

The RSE configuration might look something like:

| RSE name   | Geographic location | LFN2PFN type  | LFN2PFN name  |
| :---       | :---:               | :---: | :---:|
| `LIGO-CIT-STAGING` | Pasadena | Non-deterministic | - |
| `LIGO-CIT`         | Pasadena | Deterministic     | `igwn_schema` |
| `LIGO-WA-STAGING` | Hanford | Non-deterministic | - |
| `LIGO-WA`         | Hanford | Deterministic     | `igwn_schema` |
| `LIGO-LA-STAGING` | Livingston | Non-deterministic | - |
| `LIGO-LA`         | Livingston | Deterministic     | `igwn_schema` |

where:

* Every site producing data has an RSE to stage that data into the system
* The `igwn_schema` LFN2PFN algorithm checks to see if it knows what to do with
the data type: if SFT, construct LFN from PFN; if frame, check if it is
supported and fall-back to simple lookup if not.

For speed, the deterministic RSE should probably be the authoritative source
for datafind queries: suggests we keep e.g. `LIGO-CIT` up to date with
`LIGO-CIT-STAGING` but not necessarily vice versa.

### Fully deterministic LFN2PFN

Ideally, we would just write a python script which converts a basename into a
path.  This is both difficult and impossible to do for all files:

1. SFT basenames do not encode sufficient information.
1. There are *many* exceptions across all observing runs in how files are placed.

### Mix: non-deterministic / deterministic LFN2PFN algorithm

The easiest solution is to use a semi-deterministc LFN2PFN:

1. Register files at a non-deterministic RSE.
1. Strip the prefix from that RSE.
1. Use the remaining path for other RSEs.

!!! example "semi-deterministc lfn2pfn"

    Use the path at one RSE to place files at all RSEs:

    <!-- markdownlint-disable MD013 -->
    ```console
    CIT: /archive/frames/ER8/hoft_C02/H1/H-H1_HOFT_C02-11262/H-H1_HOFT_C02-1126256640-4096.gwf
    ICRR: /gpfs/data/ligo/ER8/hoft_C02/H1/H-H1_HOFT_C02-11262/H-H1_HOFT_C02-1126256640-4096.gwf
    CNAF: /srm/files/ligo/ER8/hoft_C02/H1/H-H1_HOFT_C02-11262/H-H1_HOFT_C02-1126256640-4096.gwf
    ```

The downside is this forces us to use multiple RSEs to represent the same
location: files replicated from ICRR to CIT will need to go to a logically
distinct RSE from files produced at CIT, resulting in something like:

| RSE name   | Geographic location | LFN2PFN type  | LFN2PFN name  |
| :---       | :---:               | :---: | :---:|
| `LIGO-CIT-ARCHIVE` | Pasadena | Non-deterministic | - |
| `LIGO-CIT-EXTERNAL`| Pasadena | Deterministic | `origin_schema` |
| `LIGO-CIT`| Pasadena | Deterministic | `igwn_schema` |
| `LIGO-WA-ARCHIVE` | Hanford | Non-deterministic | - |
| `LIGO-WA-EXTERNAL`| Hanford | Deterministic | `origin_schema` |
| `LIGO-WA`| Hanford | Deterministic | `igwn_schema` |
| `LIGO-LA-ARCHIVE` | Livingston | Non-deterministic | - |
| `LIGO-LA-EXTERNAL`| Livingston | Deterministic | `origin_schema` |
| `LIGO-LA`| Livingston | Deterministic | `igwn_schema` |
| `ICRR-ARCHIVE` | Tokyo | Non-deterministic | - |
| `ICRR-EXTERNAL`| Tokyo | Deterministic | `origin_schema` |
| `ICRR`| Tokyo | Deterministic | `igwn_schema` |

where:

* `origin_schema`: pfn is determined by path at the file origin (i.e., where it
  was originally registered on a non-deterministc RSE).
* `igwn_schema`: pfn is determined purely from the file name.

In this scheme, listing all files at a specific RSE will be misleading / meaningless.

The `origin_schema` set up also requires us to look up the PFN at e.g. Caltech
every time we want a path (additional database queries).  Every
non-deterministic PFN is also stored in the database, so the database grows in
size every time we add a file like this.

#### SFTs

In this scheme, SFTs must be given an LFN distinct from their basename, using
metadata parsed from the PFN or supplied at registration time.

### LFNs derived from PFN-based names

:no_entry: This is a no go :no_entry:

While appealingly simple, this approach will *only* work when registering data
which already follows the Caltech directory schema: GEO, Virgo and KAGRA data
created at other sites will likely follow its own directory schema.  While it
is natural in Rucio to map a specific LFN to a variety of PFNs, the LFN (and
scope) is the unique identifier for a file.

In this scheme, instead of trying to map file basenames to a full PFN, we'll
scope files by project and data format and use the full path after a fixed
prefix as the LFN.

E.g. we'll move from `<obs-run>:<basename>`:

<!-- markdownlint-disable MD013 -->
    PFN: ER8/hoft_C01_4kHz/H1/H-H1_HOFT_C01_4kHz-11265/H-H1_HOFT_C01_4kHz-1126514688-4096.gwf
    LFN: ER8:H-H1_HOFT_C01_4kHz-1126514688-4096.gwf 

To `<project.account>:<pfn'>`, where `pfn'` uses a different separator:

<!-- markdownlint-disable MD013 -->
    PFN: ER8/hoft_C01_4kHz/H1/H-H1_HOFT_C01_4kHz-11265/H-H1_HOFT_C01_4kHz-1126580224-4096.gwf
    LFN: LIGO.frames:ER8.hoft_C01_4kHz.H1.H-H1_HOFT_C01_4kHz-11265.H-H1_HOFT_C01_4kHz-1126580224-4096.gwf

LFNs will use the unique part of the PFN, with periods replacing slashes as the
separator.

Datasets will, as before, be comprised of a single instrument files and
represent the entire duration of an observing run, since this is generally the
atomic dataset we need to replicate (note that this does not prevent the
construction of additional datasets covering specific epochs).

For example:

<!-- markdownlint-disable MD013 -->
    [root@rucio rucio]# rucio list-dids LIGO.frames:
    +----------------------------------+-----------------+
    | SCOPE:NAME                       | [DID TYPE]      |
    |----------------------------------+-----------------|
    | LIGO.frames:ER8.hoft_C01.H1      | DIDType.DATASET |
    | LIGO.frames:ER8.hoft_C01_4kHz.H1 | DIDType.DATASET |
    +----------------------------------+-----------------+

    [root@rucio rucio]# rucio list-content LIGO.frames:ER8.hoft_C01_4kHz.H1 | head -5
    +--------------------------------------------------------------------------------------------------+--------------+
    | SCOPE:NAME                                                                                       | [DID TYPE]   |
    |--------------------------------------------------------------------------------------------------+--------------|
    | LIGO.frames:ER8.hoft_C01_4kHz.H1.H-H1_HOFT_C01_4kHz-11259.H-H1_HOFT_C01_4kHz-1125974016-4096.gwf | FILE         |
    | LIGO.frames:ER8.hoft_C01_4kHz.H1.H-H1_HOFT_C01_4kHz-11259.H-H1_HOFT_C01_4kHz-1125978112-4096.gwf | FILE         |

This, of course, makes construction of the LFN2PFN algorithm completely
trivial.  It also ensures data products, like SFTs, which have only included
important metadata in the directory tree, rather than the basename, can be
supported with the same algorithm.

### Mix: LFNs derived from PFN-based names & basenames

In this scheme, we could use an LFN -> PFN mapping like:

1. Frame files (and others): `scope:basename -> PFN`
1. SFTs: `scope:pfn.with.substitutions -> PFN`

Pros:

* Can handle SFTs trivially
* Can use a single LFN2PFN algorithm to handle both cases - do not have a proliferation of RSEs.
* Do not have to query database for every PFN.

Cons:

* Different RSEs will have different conventions for frame placement: still
  need non-determinstic "staging"/"origin" RSEs
* Still need to write a decent frame lfn2pfn algorithm.
