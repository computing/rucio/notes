# Import, recope and rename DIDs

What I want to do:

* Populate the `dids` table in `igwn_dev` with the contents of the `dids`
  table in `igwn_old`, moving the frame DIDs into the `frames` scope and the
  SFT DIDs into the `sfts` scope.
* Create RSEs which appropriately reflect the physical distribution network.
* Populate the new `replicas` and associated tables similarly to maintain
  accurate records of where files currently exist.

and I want to do it programmatically for easy generalisation and repeatability.

## Export / Import architecture

There is no way to talk to two differente SQL schemas in the same rucio server.

As a workaround, we will use two servers:  dump data from a server configured
for the `igwn_old` schema and import it using a server configured for the
`igwn_new` schema.

!!! warning "Rucio versions"

    The `igwn_old` schema was built with `rucio==1.23.0`.  Use this version
    when working with `igwn_old`.

    The `igwn_new` schema was built with `rucio==1.24.0` Use this
    version when working on `igwn_dev`.

    Note that the docker-compose bindings for the `dev` container mean that
    these servers *will* interfere with each other so it is likely you will
    need to reinstall rucio when switching between servers (should modify the
    bindings, obviously!).

    Use the latest release for the final product.

## `gwrucio_admin` Utility

The export/import modules above are now accessible from a utility `gwrucio_admin`:

    $ gwrucio_admin --help
    usage: gwrucio_admin [-h] {export-datasets,import-datasets,init-rucio} ...

    Utility script to rescope and attach metadata for the IGWN Rucio instance

    positional arguments:
      {export-datasets,import-datasets,init-rucio}
        export-datasets     Export datasets from a legacy schema
        import-datasets     Import and rescope datasets into a new schema
        init-rucio          Initialise rucio with RSEs and synchronise metadata

    optional arguments:
      -h, --help            show this help message and exit

Initialising a new schema:

    $ gwrucio_admin init-rucio --help
    usage: gwrucio_admin init-rucio [-h] [--account ACCOUNT] rse

    positional arguments:
      rse                RSE name to add

    optional arguments:
      -h, --help         show this help message and exit
      --account ACCOUNT  Account whose limits to set

    Usage example
    ^^^^^^^^^^^^^

    To intialise metadata configuration and set up a single RSE: LIGO-CIT:

            $ gwrucio_admin init-rucio LIGO-CIT

Exporting data

<!-- markdownlint-disable MD013 -->
    $ gwrucio_admin export-datasets --help
    usage: gwrucio_admin export-datasets [-h] [--output-dir OUTPUT_DIR]
                                         [--origin-rse ORIGIN_RSE]
                                         did_pattern

    positional arguments:
      did_pattern           Glob pattern to find datasets

    optional arguments:
      -h, --help            show this help message and exit
      --output-dir OUTPUT_DIR
                            Output directory for pickle dump
      --origin-rse ORIGIN_RSE
                            Origin RSE to get PFNs from

    Usage example
    ^^^^^^^^^^^^^

    To export datasets matching DID pattern ER8:H-H1_HOFT_C01_4* and PFNs from LIGO-CIT-ARCHIVE:

        $ gwrucio_admin export-datasets ER8:H-H1_HOFT_C01_4* --output-dir /igwn-rucio-dev/ --origin-rse LIGO-CIT-ARCHIVE

Importing data:

<!-- markdownlint-disable MD013 -->
    $ gwrucio_admin import-datasets --help
    usage: gwrucio_admin import-datasets [-h] [--origin-rse ORIGIN_RSE]
                                         [--scope-scheme {content,fixed,project}]
                                         [--new-scope NEW_SCOPE]
                                         pickle_file

    positional arguments:
      pickle_file           The pickle file with the dataset dictionaries.

    optional arguments:
      -h, --help            show this help message and exit
      --origin-rse ORIGIN_RSE
                            RSE to register initial replicas at (default: LIGO-
                            CIT-LEGACY)
      --scope-scheme {content,fixed,project}
                            New scope scheme to use
      --new-scope NEW_SCOPE
                            scope to move datasets to

    Usage example
    ^^^^^^^^^^^^^

    To import datasets and rescope with the "project" scope scheme:

            $ gwrucio_admin import-datasets  /igwn-rucio-dev/ER8\:H-H1_HOFT_C01_4kHz\&H-H1_HOFT_C01.p --scope-scheme project

## Walkthrough

Assumes you have already run through the [basic
setup](https://rucio.docs.ligo.org/notes/dev/) and have database dumps of the
old and new schemas for the production instance and an initialized but empty
instance, respectively.

Bring up development environment:
    $  docker-compose --file ~/src/rucio/etc/docker/dev/docker-compose-storage.yml up -d

Which brings up:

* Postgres database `dev_ruciodb_1` with populated `igwn_old` schema (snapshot
  of production state) and initialized but empty `igwn_dev` schema
* Rucio server `dev_rucio-igwn_dev_1`: points at `igwn_dev` schema
* Rucio server `dev_rucio-igwn_old_1`: points at `igwn_old` server

### Dump some datasets from `igwn_old`

Log into the old server and intall `gwrucio_admin`:

    $  docker exec -it dev_rucio-igwn_old_1 /bin/bash
    [root@rucio rucio]# pip install /igwn-rucio-dev/igwn-rucio-admin/

Dump some datasets:

<!-- markdownlint-disable MD013 -->
    [root@rucio rucio]# gwrucio_admin export-datasets ER8:H-H1_HOFT_C0* --output-dir /igwn-rucio-dev/ --origin-rse LIGO-CIT-ARCHIV
    <snip>
    2021-03-05 21:50:42,119 259 INFO  Saving to /igwn-rucio-dev/ER8:H-H1_HOFT_C01_4kHz&H-H1_HOFT_C02&H-H1_HOFT_C00&H-H1_HOFT_C01.p
    2021-03-05 21:50:42,139 259 INFO  /igwn-rucio-dev/ER8:H-H1_HOFT_C01_4kHz&H-H1_HOFT_C02&H-H1_HOFT_C00&H-H1_HOFT_C01.p is 1.73 MB
    2021-03-05 21:50:42,139 259 INFO  Export complete
    2021-03-05 21:50:42,142 259 INFO  Walltime: 20.44 sec.

### Initialise accounts in `igwn_new`

Suppose we want additional accounts `frames` and `sfts`:

<!-- markdownlint-disable MD013 -->
    rucio-admin account add --type GROUP --email james.clark@ligo.org frames
    rucio-admin account add --type GROUP --email james.clark@ligo.org sfts

    rucio-admin identity add --account frames --type USERPASS --id "admin" --password "2coalesceornot" --email james.clark@ligo.org
    rucio-admin identity add --account frames --type SSH --id "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDYmEiAWS02Iwsl48U80Nu9cmigyQzns1FTen0gSd3p2FnyAsX4Xfhr27jPFCUGCDiA1uMnRwT7+IXtTNr0Wzbz7fGi6/3jHxbdunr5bgj0sCHG+/7W/w4Mjf7JFR6+uKDwG5AVAbwp4AoGSgVD+o88ThvlaEacBcrSR96pdPuRWM8tXvAVKFNzY0cZ16cqLbxH2z95QS+0Yw5aOhrsWx02V+t6vg9vbyPE2SeBv9A++kzUkTukP8yk44zt95MWbkA9pawhaK0RLU2cEFGIDShTk44D9XeZtXZLebS/0Lwd+2lQKzRWQggbkoAbFrtOAsL9m/FIxAVpDSQzMYB0rQBP+aMt67ywrk6bykh9XYbpuc8mEZpQXF5TRPknThD3spvl2AJKI97MH1ioT1lsnuiqPMKDZLO6t977sfsPuJqfFd4selM4sIglZd+9ROgntIm0XKD+p2kXn46M9L6dBSVyMM6f+P6VfaqCTv5NlbZkKdbhNx8L/nQoK/1wfhMVTnBEseLd5DmCQjPdmuHFPV9XNPNQGb7KAPGpmWsgZmn7MWcE4EP7py/MPv5Mt4QCQGv/o7Ev5/yBs7glopv9niuZV/+6dv1537iEJdxp+4zrdltd4Wq4/iA6r/SNrbGs8gBK1JOUS4sasC3RzDztFjavJCJKrglvgNoyG/tUTzdaqQ== james.clark@ligo.org" --email james.clark@ligo.org
    rucio-admin account add-attribute --key 'admin' --value true frames

    rucio-admin identity add --account sfts --type USERPASS --id "admin" --password "2coalesceornot" --email james.clark@ligo.org
    rucio-admin identity add --account sfts --type SSH --id "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDYmEiAWS02Iwsl48U80Nu9cmigyQzns1FTen0gSd3p2FnyAsX4Xfhr27jPFCUGCDiA1uMnRwT7+IXtTNr0Wzbz7fGi6/3jHxbdunr5bgj0sCHG+/7W/w4Mjf7JFR6+uKDwG5AVAbwp4AoGSgVD+o88ThvlaEacBcrSR96pdPuRWM8tXvAVKFNzY0cZ16cqLbxH2z95QS+0Yw5aOhrsWx02V+t6vg9vbyPE2SeBv9A++kzUkTukP8yk44zt95MWbkA9pawhaK0RLU2cEFGIDShTk44D9XeZtXZLebS/0Lwd+2lQKzRWQggbkoAbFrtOAsL9m/FIxAVpDSQzMYB0rQBP+aMt67ywrk6bykh9XYbpuc8mEZpQXF5TRPknThD3spvl2AJKI97MH1ioT1lsnuiqPMKDZLO6t977sfsPuJqfFd4selM4sIglZd+9ROgntIm0XKD+p2kXn46M9L6dBSVyMM6f+P6VfaqCTv5NlbZkKdbhNx8L/nQoK/1wfhMVTnBEseLd5DmCQjPdmuHFPV9XNPNQGb7KAPGpmWsgZmn7MWcE4EP7py/MPv5Mt4QCQGv/o7Ev5/yBs7glopv9niuZV/+6dv1537iEJdxp+4zrdltd4Wq4/iA6r/SNrbGs8gBK1JOUS4sasC3RzDztFjavJCJKrglvgNoyG/tUTzdaqQ== james.clark@ligo.org" --email james.clark@ligo.org
    rucio-admin account add-attribute --key 'admin' --value true sfts

Remember to switch to the appropriate account when importing data

### Import datasets to `igwn_new`

Enter the container and install `gwrucio_admin`:

    $  docker exec -it dev_rucio-igwn_dev_1 /bin/bash
    [root@rucio rucio]# pip install /igwn-rucio-dev/igwn-rucio-admin/

#### Fixed scope

Import datasets into a scope `LIGO.frames` and create datasets, using the old
observing run scope and previous content type:

<!-- markdownlint-disable MD013 -->
    [root@rucio rucio]# gwrucio_admin import-datasets /igwn-rucio-dev/ER8\:H-H1_HOFT_C01_4kHz\&H-H1_HOFT_C02\&H-H1_HOFT_C00\&H-H1_HOFT_C01.p --scope-scheme fixed --new-scope LIGO.frames
    [root@rucio rucio]# rucio list-dids LIGO.frames:*
    +----------------------------------+-----------------+
    | SCOPE:NAME                       | [DID TYPE]      |
    |----------------------------------+-----------------|
    | LIGO.frames:ER8.H1_HOFT_C00      | DIDType.DATASET |
    | LIGO.frames:ER8.H1_HOFT_C01      | DIDType.DATASET |
    | LIGO.frames:ER8.H1_HOFT_C01_4kHz | DIDType.DATASET |
    | LIGO.frames:ER8.H1_HOFT_C02      | DIDType.DATASET |
    +----------------------------------+-----------------+
