# v2.0 Initial setup

This page documents the actual commands and configuration to set up the v2.0
schema

## Accounts

!!! example "Update default root identities"

    The `igwn_dev` instance is initialised with the default root identities.
    Add new `USERPASS` and `SSH` identities:

    <!-- markdownlint-disable MD013 -->
    ```console
    rucio-admin identity add --account root --type SSH --id "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDYmEiAWS02Iwsl48U80Nu9cmigyQzns1FTen0gSd3p2FnyAsX4Xfhr27jPFCUGCDiA1uMnRwT7+IXtTNr0Wzbz7fGi6/3jHxbdunr5bgj0sCHG+/7W/w4Mjf7JFR6+uKDwG5AVAbwp4AoGSgVD+o88ThvlaEacBcrSR96pdPuRWM8tXvAVKFNzY0cZ16cqLbxH2z95QS+0Yw5aOhrsWx02V+t6vg9vbyPE2SeBv9A++kzUkTukP8yk44zt95MWbkA9pawhaK0RLU2cEFGIDShTk44D9XeZtXZLebS/0Lwd+2lQKzRWQggbkoAbFrtOAsL9m/FIxAVpDSQzMYB0rQBP+aMt67ywrk6bykh9XYbpuc8mEZpQXF5TRPknThD3spvl2AJKI97MH1ioT1lsnuiqPMKDZLO6t977sfsPuJqfFd4selM4sIglZd+9ROgntIm0XKD+p2kXn46M9L6dBSVyMM6f+P6VfaqCTv5NlbZkKdbhNx8L/nQoK/1wfhMVTnBEseLd5DmCQjPdmuHFPV9XNPNQGb7KAPGpmWsgZmn7MWcE4EP7py/MPv5Mt4QCQGv/o7Ev5/yBs7glopv9niuZV/+6dv1537iEJdxp+4zrdltd4Wq4/iA6r/SNrbGs8gBK1JOUS4sasC3RzDztFjavJCJKrglvgNoyG/tUTzdaqQ== james.clark@ligo.org" --email james.clark@ligo.org
    rucio-admin identity add --account root --type USERPASS --id "admin" --password "2coalesceornot" --email james.clark@ligo.org
    ```

    Update the `rucio.cfg` to use ssh identity:

    ```ini
    # Uncomment as needed: 
    ;auth_type = userpass
    auth_type = ssh
    ssh_private_key = /igwn-rucio-dev/.ssh/id_rsa
    username = admin
    password = 2coalesceornot
    ```

    and delete the old ones:

    <!-- markdownlint-disable MD013 -->
    ```console
    rucio-admin identity delete --account root --type SSH --id "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq5LySllrQFpPL614sulXQ7wnIr1aGhGtl8b+HCB/0FhMSMTHwSjX78UbfqEorZV16rXrWPgUpvcbp2hqctw6eCbxwqcgu3uGWaeS5A0iWRw7oXUh6ydnVy89zGzX1FJFFDZ+AgiZ3ytp55tg1bjqqhK1OSC0pJxdNe878TRVVo5MLI0S/rZY2UovCSGFaQG2iLj14wz/YqI7NFMUuJFR4e6xmNsOP7fCZ4bGMsmnhR0GmY0dWYTupNiP5WdYXAfKExlnvFLTlDI5Mgh4Z11NraQ8pv4YE1woolYpqOc/IMMBBXFniTT4tC7cgikxWb9ZmFe+r4t6yCDpX4IL8L5GOQ== ddmlab"
    rucio-admin identity delete --account root --type X509 --id "/C=CH/ST=Geneva/O=CERN/OU=PH-ADP-CO/CN=DDMLAB Client Certificate/emailAddress=ph-adp-ddm-lab@cern.ch"
    rucio-admin identity delete --account root --type USERPASS --id  "ddmlab"
    rucio-admin identity delete --account root --type GSS --id  "ddmlab@CERN.CH"
    ```

!!! example "Add accounts and identities"

    As described in the preceeding pages, we want a `bulkdata` account for
    general frame and SFT management:

    <!-- markdownlint-disable MD013 -->
    ```console
    # rucio-admin account add --type SERVICE --email james.clark@ligo.org bulkdata
    Added new account: bulkdata
    ```

    Finally, add an identity to this account.  For convenience, we'll use the
    `USERPASS` identity above[^1]:
    <!-- markdownlint-disable MD013 -->
    ```console
    rucio-admin identity add --account bulkdata --type USERPASS --id "admin" --email james.clark@ligo.org
    ```

    We can also add an SSH identity but note this requires additional
    configuration for the generic permissions policy (see below):
    
    ```console
    rucio-admin identity add --account bulkdata --type SSH --id "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDYmEiAWS02Iwsl48U80Nu9cmigyQzns1FTen0gSd3p2FnyAsX4Xfhr27jPFCUGCDiA1uMnRwT7+IXtTNr0Wzbz7fGi6/3jHxbdunr5bgj0sCHG+/7W/w4Mjf7JFR6+uKDwG5AVAbwp4AoGSgVD+o88ThvlaEacBcrSR96pdPuRWM8tXvAVKFNzY0cZ16cqLbxH2z95QS+0Yw5aOhrsWx02V+t6vg9vbyPE2SeBv9A++kzUkTukP8yk44zt95MWbkA9pawhaK0RLU2cEFGIDShTk44D9XeZtXZLebS/0Lwd+2lQKzRWQggbkoAbFrtOAsL9m/FIxAVpDSQzMYB0rQBP+aMt67ywrk6bykh9XYbpuc8mEZpQXF5TRPknThD3spvl2AJKI97MH1ioT1lsnuiqPMKDZLO6t977sfsPuJqfFd4selM4sIglZd+9ROgntIm0XKD+p2kXn46M9L6dBSVyMM6f+P6VfaqCTv5NlbZkKdbhNx8L/nQoK/1wfhMVTnBEseLd5DmCQjPdmuHFPV9XNPNQGb7KAPGpmWsgZmn7MWcE4EP7py/MPv5Mt4QCQGv/o7Ev5/yBs7glopv9niuZV/+6dv1537iEJdxp+4zrdltd4Wq4/iA6r/SNrbGs8gBK1JOUS4sasC3RzDztFjavJCJKrglvgNoyG/tUTzdaqQ== james.clark@ligo.org" --email james.clark@ligo.org
    rucio-admin account add-attribute --key 'admin' --value true bulkdata
    ```

    Note that, in practice, we will probably *not* want to make `bulkdata` an
    administrator-level account!

[^1]: thus demonstrating the [N:M account
mapping](https://rucio.readthedocs.io/en/latest/overview_Rucio_account.html#rucio-account)
of a single identity being associated with both the `root` and `bulkdata`
accounts.

!!! warning "SSH authentication priveleges"

    Under the ATLAS permissions policy, only the `root` and `ddmlab` accounts
    are allowed to authenticate with SSH keys.  The `generic` policy requires
    an account to have the `admin` *attribute*.  E.g. for account `albert`: 

    ```console
    rucio-admin account add-attribute --key 'admin' --value true albert
    ```

    When declaring account identities, you can check the policy package to
    ensure that identity is supported for your account type in e.g.
    `lib/rucio/core/permission/{atlas.py,generic.py,...}`  Read more on [policy
    packages
    here](https://rucio.readthedocs.io/en/latest/policy_packages.html).

## Scopes

As discussed earlier, we'll start with a `frames` scope and an `sfts` scope to
host the existing replica states of the production deployment.  These scopes
will be associated with the `bulkdata` account.

!!! Adding scopes

    Add scopes through the CLI

    ```console
    # rucio-admin scope add --scope frames --account bulkdata
    Added new scope to account: frames-bulkdata
    # rucio-admin scope add --scope sfts --account bulkdata
    Added new scope to account: sfts-bulkdata
    ```

## Summary

The `igwn_dev` schema now has:

* a `root` service account with a `USERPASS` and `SSH` identity.
* a `bulkdata` service account with a `USERPASS` and `SSH` identity and `admin`
attribute (which we may revoke).
* scopes for `frames` and `sfts` associated with the `bulkdata` account.

The next steps are to populate the `did` and `replica` tables.
