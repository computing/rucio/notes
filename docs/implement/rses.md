# RSE Network

In the previous section, we proposed a registration & replication procedure in
which we register files as they are produced at non-deterministic RSEs and
replicate files to deterministic RSEs.

E.g.:

<!-- markdownlint-disable MD013 -->
| **RSE name**   | **Geographic location** | **LFN2PFN type**  | **LFN2PFN name**  |
| :---       | :---:               | :---: | :---:|
| `LIGO-CIT-STAGING` | Pasadena | Non-deterministic | - |
| `LIGO-CIT`         | Pasadena | Deterministic     | `igwn_schema` |
| `LIGO-WA-STAGING` | Hanford | Non-deterministic | - |
| `LIGO-WA`         | Hanford | Deterministic     | `igwn_schema` |
| `LIGO-LA-STAGING` | Livingston | Non-deterministic | - |
| `LIGO-LA`         | Livingston | Deterministic     | `igwn_schema` |

where:

* Every site producing data has an RSE to push that data into the system
* The `igwn_schema` LFN2PFN algorithm checks to see if it knows what to do with
the data type: if SFT, construct LFN from PFN; if frame, check if it is
supported and fall-back to simple lookup if not.

Our objectives on this page:

* Specify the full list of RSEs required to reproduce current production state.
* Define names and paths for each RSE

## Current RSE configuration

Currently have:

| **RSE name**         | **Purpose**  | **Prefix** | **lfn2pfn** |
|:---              | :---:    | :---:  | :---:   |
| ATLAS            | RECEIVE  | `/atlas/bute2/rucio`| `ligo_legacy` |
| ATLAS-SRC        | SOURCE    | `/atlas/hsm` | non-deterministic |
| CNAF             | RECEIVE  | `/virgoplain/rucio` | `ligo_legacy` |
| ICRR             | RECEIVE  | `/data/hl` |  `ligo_legacy` |
| ICRR-LVK         | RECEIVE  | `/data/lvk` | `hash` |
| ICRR-STAGING     | SOURCE    | `/data/proc` | non-deterministic |
| LIGO-CIT         | RECEIVE  | `/archive/frames` | `igwn` |
| LIGO-CIT-ARCHIVE | SOURCE    | `/archive/frames` | `ligo_legacy` |
| LIGO-CIT-HASH    | TEST     | `/mnt/rucio/hash` | `hash` |
| LIGO-CIT-SFTS    | SOURCE (SFTs only)  | `/archive/frames` | non-deterministic |
| LIGO-WA          | RECEIVE  | `/archive/frames` | `ligo_legacy` |
| LIGO-WA-ARCHIVE  | SOURCE    | `/archive/frames` | `ligo_legacy` |
| LIGO-WA-HASH     | TEST     | `/home/rucio/hash` | `hash` |
| OZSTAR           | RECEIVE  | `/datasets/LIGO/private` | `ligo_legacy` |
| UWM              | TEST     | `/home/rucio/hashed` | `hash` |

* "SOURCE": source of replicas pushed *into* Rucio from an archival location
* "RECEIVE": receive replicas from other locations
* ICRR-LVK is no longer actually used since they were just coping data into a
new location with the Caltech directory structure, anyway.

## Proposed RSE configuration

We'll set something similar up but note:  "Staging" has a specific meaning /
function in Rucio; we should avoid that term.

| **Physical location** | **Purpose** | **Logical name**   | **Prefix**                   | **lfn2pfn** |
| :---              | :---:   | :---:          | :---:                    | :---:   |
| Hannover (Atlas)  | SOURCE  | `ATLAS-SRC`    | `/atlas/hsm`             | non-deterministic |
| Hannover (Atlas)  | RECEIVE | `ATLAS-DST`    | `/atlas/bute2/rucio`     | `igwn`            |
| Bologna (CNAF)    | RECEIVE | `CNAF-DST`     | `/virgoplain/rucio`      | `igwn`            |
| Caltech           | SOURCE  | `LIGO-CIT-SRC` | `/archive/frames`        | non-deterministic |
| Caltech           | RECEIVE | `LIGO-CIT-DST` | `/archive/frames`        | `igwn`            |
| Hanford           | SOURCE  | `LIGO-WA-SRC`  | `/archive/frames`        | non-deterministic |
| Hanford           | RECEIVE | `LIGO-WA-DST`  | `/archive/frames`        | `igwn`            |
| Swinburne         | RECEIVE | `SUT-OzStar`   | `/datasets/LIGO/private` | `igwn`            |
| Tokyo             | SOURCE  | `ICRR-SRC`     | `/data/proc`             | non-deterministic |
| Tokyo             | RECEIVE | `ICRR-DST`     | `/data/hl`               | `igwn`            |
