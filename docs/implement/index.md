# Implementing the new schema

This section discusses the internal implementation of the new naming schema,
including:

* RSE configuration
* LFN2PFN algorithm
* Restoring replica state

This work is still based in the local workstation `docker-compose` environment
of the `igwn-dev` system.
