# LFN2PFN

In the previous section, we proposed a registration & replication procedure in
which we register files as they are produced at non-deterministic RSEs and
replicate files to deterministic RSEs.

Scopes will be based around project and data type, e.g. `LIGO.frames`,
`Virgo.sfts` etc.

However, our legacy (and future) frame and sft paths do still include metadata
which is not easily parsed directly from file basenames.  This page describes
our requirements and the construction of a simple python code to convert
frame and SFT file names to physical filenames using only the basename and some
minimal additional metadata.

## Executive summary

Note:

* RSE: Rucio storage element; logically distinct storage locations.
* PFN: Physical filename; the path to a file at an RSE.
* LFN: Logical filename; a logical identifier for a file in Rucio.
* DID: data identifier; the *unique* identifier for a file in the rucio database.
  Follows the form: `<scope>:<logical filename>`.

### The problem

RSEs may be:

1. "Deterministic": physical filenames at the RSE are determined
   algorithmically from the unique identifer which represents that file.  The
   path to the file is computed client-side on request.  Different RSEs can
   have different algorithms to conver the logical filename to the physical
   filename.
1. "Non-deterministic": physical filenames *at that RSE* are written into the
   backend database.  If files are replicated to a new non-determinstic RSE,
   their path is determined from one of a handful of simple [functions in the Rucio
   src](https://github.com/rucio/rucio/blob/master/lib/rucio/common/utils.py#L616).

Rucio supports hot-pluggable deterministic algorithms: 3rd-party python
functions written by the community using Rucio (i.e. us).  The default
algorithm is an efficient hash string which is not Human-readable.

Rucio does *not* simply store a physical filename to be used when replicating
files. Instead, the pfn is, one way or another, computed from the logical file
name.  This is vastly more efficient than querying the database and allows
total flexibility about file placement at different RSEs.

Our users expect frame files and SFTs to live in very specific paths which,
after a change in prefix, are identical at all storage locations.  in
principle, this is unnecessary if everyone used Rucio and/or a local
`gwdatafind` server; in practice, most users are POSIX and have existing
expectations.

**Our goal** is to be able to replicate files from e.g. CIT to other end-points,
like OzSTAR, while reproducing the path at CIT.

**Our problem** is the paths to archival data are not trivially determined from
the logical filename, usually just taken to be the basename.

### Current approach

In the current production instance, we have registered frame files in Rucio
using their basename as the identifying database key (Data Identifier; DID).

The paths to IGWN frame and SFT datasets, particularly older ones, have
irregular naming schemes: their path on disk cannot be easily determined from
the basename.

For frames, we have used a deterministic algorithm with an increasingly
byzantine series of hard-coded if/else exceptions.  This is unsustainable and
anyway requires significant modification when we change scopes.

For SFTs, we registered files at a non-deterministic RSE and, to place files at
other RSEs, query the database for the path at the non-determinstic RSE and use
that  This only works for data at non-deterministic RSEs, which grows the
database, and involves database queries for every single file path; not a
catastrophe but far the intended usage model.

Additionally, SFT basenames are not unique, so the DID in the database is
constructed from user-supplied metadata when the file is registered (but
otherwise follows the standard LDAS naming scheme).

### Alternative solutions

The rest of this page goes into some detail about other ways we might configure
deterministic / non-deterministic RSEs and construct LFN-to-PFN algorithms.
Here's a summary of options I currently have in mind:

1. "Ideal": Use a deterministic algorithm based on the file basename.
1. "Separators": for the DID, use the full, unique path of the files, with `/`
   replaced by `.`.
1. "Lookup": Use a lookup table to find the non-deterministic part of a PFN, in cases
   where another part can be parsed from the logical filename (this is true for
   frame files).
1. "diskcache": Abandon Rucio's intended usage and read paths from a diskcache.

Each have problems:

* The "ideal" case simply does not work for older frame datasets or SFTs.
* The "separators" approach only works for files if they are registered
in-place - it's great for SFTs and frame files already in the archival location
at CIT but would not work for Virgo/KAGRA/GEO frames, produced at their
respective sites.  Tying the PFN so tightly to the LFN also makes changes in
location non-trivial; changes in the PFN would introduce exceptions into the
LFN2PFN algorithm or total removal and re-registration of entire datasets.
* Using a lookup table effectively means maintaining a whole additional
database!
* Diskcache requires an up-to-date copy of each site's diskcache dump to be
available wherever the lfn2pfn is required.  This should be straightforward on
the server side but user clients would struggle.  Maybe it'd be sufficient on
the server, however?  What happens on a local client if the lfn2pfn algorithm
cannot be found - can it be forced to get it from the server?

Note that an RSE can have one, and only one, lfn2pfn algorithm.  There is no
limit, per se, on the number of RSEs, and they can all point to the same
physical location.

## LFN2PFN algorithms

### The ideal case

The best situation would be one in which a file's basename maps directly, and
trivially, to the path like:

<!-- markdownlint-disable MD013 -->
    L-L1_HOFT_C01-1132355584-4096.gwf -> L1/L1_HOFT_C01/11323/L-L1_HOFT_C01-1132355584-4096.gwf

Instead, we have e.g.:

<!-- markdownlint-disable MD013 -->
    L-L1_HOFT_C01-1132355584-4096.gwf -> O1/hoft_C01/L1/L-L1_HOFT_C01-11323/L-L1_HOFT_C01-1132355584-4096.gwf

Where:

* The `O1` parent is totally unspecified by the basename (we cannot just look
  up the epoch as that does not always map directly to the frame directory -
  e.g. `O3GK` vs `postO3`)
* Conventions for upper and lower case, and even the position of parts of the
  "content" vary across frame types.

Modern frame directories do obey more rigorous naming conventions, however -
the recent KAGRA frames follow a completely deterministic path, modulo the
inclusion of an `O3GK`.

**Propose**:

1. use a simple deterministic algorithm for new frame files
1. Make a decision about where the observing run prefix comes from - may make
   sense to configure e.g. O3-specific RSEs?

### Diskcache

Example call using old LDR module:

<!-- markdownlint-disable MD013 -->
    python -m ldr.diskcache -r G-G1_RDS_C01_L3 -m 1304885640 -M $((1304885640+60-1)) /data/GEO-ldas-grid.ldas
    /archive/frames/A6/L3/GEO/G-G1_RDS_C01_L3-13048/G-G1_RDS_C01_L3-1304885640-60.gwf

Not sure how well, if at all, this works with SFTs - important metadata is only
encoded in the full PFN.  We probably couldn't use basenames at least.

Indeed, in the case of SFTs, where the calibration information is not contained
in the content string, we get multiple returns:

<!-- markdownlint-disable MD013 -->
    (gwrucio) bash-4.2# python -m ldr.diskcache -r H-1_H1_1800SFT_O2-1164 -m 1164674947 -M $((1164674947+1800-1)) ./sft_cache_dump
    /archive/frames/O2/pulsar/sfts/tukeywin/LHO_C00/H-1_H1_1800SFT_O2-1164/H-1_H1_1800SFT_O2-1164674947-1800.gwf
    /archive/frames/O2/pulsar/sfts/tukeywin/LHO_C01/H-1_H1_1800SFT_O2-1164/H-1_H1_1800SFT_O2-1164674947-1800.gwf
    /archive/frames/O2/pulsar/sfts/tukeywin/LHO_C02/H-1_H1_1800SFT_O2-1164/H-1_H1_1800SFT_O2-1164674947-1800.gwf

We can, however, include the calibration in the regex if we're smart:

<!-- markdownlint-disable MD013 -->
    (gwrucio) bash-4.2# python -m ldr.diskcache -r ".*C02.*H-1_H1_1800SFT_O2" -m 1164674947 -M $((1164674947+1800-1)) ./sft_cache_dump
    /archive/frames/O2/pulsar/sfts/tukeywin/LHO_C02/H-1_H1_1800SFT_O2-1164/H-1_H1_1800SFT_O2-1164674947-1800.gwf

Slightly concerned this takes a long time to evaluate, however:

<!-- markdownlint-disable MD013 -->
    (gwrucio) bash-4.2# time python -m ldr.diskcache -r ".*C02.*H-1_H1_1800SFT_O2" -m 1164674947 -M $((1164674947+1800-1)) ./sft_cache_dump
    /archive/frames/O2/pulsar/sfts/tukeywin/LHO_C02/H-1_H1_1800SFT_O2-1164/H-1_H1_1800SFT_O2-1164674947-1800.gwf

    real  0m2.442s
    user  0m1.147s

We can improve this with:

<!-- markdownlint-disable MD013 -->
    (gwrucio) bash-4.2# time python -m ldr.diskcache -r "(?s)^.*C02.*H-1_H1_1800SFT_O2" -m 1164674947 -M $((1164674947+1800-1)) ./sft_cache_dump
    /archive/frames/O2/pulsar/sfts/tukeywin/LHO_C02/H-1_H1_1800SFT_O2-1164/H-1_H1_1800SFT_O2-1164674947-1800.gwf

    real  0m0.433s
    user  0m0.122s
    sys 0m0.111s

So a diskcache-based solution can work for SFTs **if** you know the calibration
but, one way or another, that's going to have to be part of the LFN.

### GWDataFind client

[GWDataFind](https://git.ligo.org/gwdatafind/gwdatafind) is a completely
independent server-client system with full access to frame paths from a local
diskcache.

In cases where frames already exist on disk, we could just query the local
datafind service.

Some of the more obvious issues:

1. Can we actually access gwdatafind servers from outside?  They generally
   serve data over http (port 80) and are not typically explosed, as far as I
   know.
1. This would require authentication - I believe this should be straightforward
   with a keytab, subject to limitations above.
1. This will only ever work for replicating files from an end point with a
   datafind server.  For non-LIGO detectors where frames live in different
   PFNs, we can *register* those frames at non-deterministic RSEs but their
   *placement* will still require a deterministic LFN2PFN algorithm.

### PFNs from lookup tables

For frames, the basic problem is we have paths like
`ER8/hoft_C02/H1/H-H1_HOFT_C02-11265/H-H1_HOFT_C02-1126539264-4096.gwf`
where the `ER8/hoft_C02/H1` cannot be easily determined from the basename.  We
can, however, easily get the immediate parent directory `H-H1_HOFT_C02-11265`.

So how about a lookup table, in the form of a python dictionary, keyed by the
frame identifiers like `H-H1_HOFT_C02-11265` with values equal to the prefix in
which that path can be found?

Pros:

* Super easy to implement - the dictionary can be stored as json data in the
  lfn2pfn package (it's only a few 100 kb).
* Fast, client-side operation.

Cons:

* Totally unfeasible for *new* data - this is strictly to handle the
  irregularly-named archival frame files.
* Only suitable for frame files - does not solve the non-uniqueness of SFT
  names, plus SFT directory hierarchies are even more irregular.

### PFNs from LFNs with separators

Simplest algorithm is something like:

<!-- markdownlint-disable MD013 -->
| LFN | PFN |
| :--- | :---: |
| `O1.hoft_C01.L1.L-L1_HOFT_C01-11323.L-L1_HOFT_C01-1132355584-4096.gwf` | `O1/hoft_C01/L1/L-L1_HOFT_C01-11323/L-L1_HOFT_C01-1132355584-4096.gwf` |

This obviously only works if we want to replicate to those paths everywhere and
never want to move files.  It is, however, our best option for SFTs.

**This is our fundamental problem: we need to cater for paths across experiments (LIGO, Virgo, KAGRA, GEO600**).

### PFNs from archival paths

1. Register data at a non-deterministic RSE
1. Query db for non-deterministic PFN for placement at deterministic RSEs

This is just a very inefficient version of the previous algorithm.

### PFNs from metadata

Ideal case:

* Determine PFN from the metadata catalog and the basename.
* Previously found that this only works AFTER a file has been registered.
* PFNs from metadata only works once the metadata exists in the rucio catalog.
* Registering a replica at a deterministic RSE *uses* the lfn2pfn algorithm -
  you cannot simply pass the expected PFN.
* The only information available to a pluggable lfn2pfn algorithm is a path
  prefix and the DID name

This is why we must use a non-deterministc RSE to stage data into rucio; once
it is there, we have access to the DID name and its metadata.

In reality, we also need to note that it will be grossly inefficient to query
the database for the metadata associated with every single file in a query.
Wherever possible, we should try to make the determination on the client side.

## DID name examples

### Frames

!!! example "Frame DID names"

    <!-- markdownlint-disable MD013 -->
    | DID        | Format | Example |
    | :---       | :---:  | :---:   |
    | Files      | `<scope>:<basename>`       | `LIGO.frames:L-L1_HOFT_C01-1132355584-4096.gwf` |
    | Datasets   | `<scope>:<obs-run>.<ifo-frame-content>` | `LIGO.frames:L1_HOFT_C01` |
    | Containers | `<scope>.<frame-content>`  | `LIGO.frames:L1_HOFT_C01` |

!!! example "Frame PFNs"

    ```console
    LIGO.frames:L-L1_HOFT_C01-1132355584-4096.gwf ->
    /hdfs/frames/O1/hoft_C01/L1/L-L1_HOFT_C01-11323/L-L1_HOFT_C01-1132355584-4096.gwf
    ```

### SFTs
