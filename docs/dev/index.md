# Development environment

Describes how to configure and start a complete `docker-compose`-based
development environment.

Rucio provides a `docker-compose` manifest to set up a [fully operational rucio
system](https://rucio.readthedocs.io/en/latest/demo_environment.html) with
dummy data.  We'll use this to replicate the current production IGWN rucio
instance and develop new procedures and schemas.

The basic idea:

1. Import a dump from the current rucio database into the PostGreSQL server
1. Add a rucio server to point at the old database
1. Add a rucio server to point at any new database

## Associated repositories

* Fork of rucio with modified `docker-compose` manifest: [feature-igwn_schema
branch](https://github.com/astroclark/rucio/tree/feature-igwn_schema)
* [igwn-rucio-dev](https://git.ligo.org/rucio/igwn-rucio-dev): additional
notes, configurations and utilties

## Setting up the development enviroment

In this section, we set up:

1. The `dev` rucio server and schema (from the main rucio repository).
1. An `igwn_old` server and schema which contains the current/recent production
   database (from which we're going to want to copy DID and replica states).
1. An `igwn_dev` server and schema which we will use to prototype our new data
   topology.

Clone both of the above repositories and make sure the `igwn-rucio-dev`
repository is listed as a volume in the [docker
manifest](https://github.com/astroclark/rucio/blob/feature-igwn_schema/etc/docker/dev/docker-compose-storage.yml)

Navigate to the root of the rucio repository clone and bring up the full rucio system:

    docker-compose --file etc/docker/dev/docker-compose-storage.yml up -d

### Initialise the `dev` instance

The rucio repository contains a collection of scripts to set up a standard test
environment with pre-defined RSEs, scopes etc.  Activate this environment like:

    $  docker exec -it dev_rucio_1 /bin/bash
    [root@rucio rucio]# tools/run_tests_docker.sh -i

The `-i` options runs `tools/reset_database.py` (drops any existing tables from
that schema and bootstraps the new database), sets up `rucio.cfg`, does the
alembic migration and sets up the RSEs (RSEs are set up with
`tools/docker_activate_rses.sh`).

Log into the psql container, set search path to see the default `dev` schema:

    $  docker exec -it dev_ruciodb_1 /bin/bash
    root@def2c188a978:/# psql -U rucio rucio
    rucio=# SET search_path='dev';
    SET
    rucio=# \dt;
                       List of relations
     Schema |             Name             | Type  | Owner
    --------+------------------------------+-------+-------
     dev    | account_attr_map             | table | rucio

### Importing the production schema

We now want to import a dump of the production database into a new schema,
called `igwn_old`.

Make sure the `igwn-rucio-dev` repository is mounted and that you have git-lfs
downloaded the production database, and in the psql container restore with:

    docker exec -it dev_ruciodb_1 /bin/bash
    psql -U rucio -d rucio < /igwn-rucio-dev/igwn_old.db

See "Renaming the production schema" if you do not have `igwn_old.db`.

!!! example "Renaming the production schema"

    The production database schema is named `public`.  To keep things clear, we
    should rename the schema from the scheduled backup and save that database
    to file for future imports.

    Copy the backup to the mounted `igwn-rucio-dev` volume and, from the psql
    container brought up by docker compose, import the dump and rename the
    schema:

    ```console
    docker exec -it dev_ruciodb_1 /bin/bash
    psql -U rucio -d rucio < /igwn-rucio-dev/rucio-db.dump.2021-01-08-18-00
    ```

    Log into psql and alter the schema name:

    ```sql
    rucio=# SET search_path='public';
    SET
    rucio=# ALTER SCHEMA public RENAME TO igwn_old
    ALTER SCHEMA;
    ```

    Finally, dump this schema for future imports:

    ```console
    pg_dump -U rucio rucio --schema "igwn_old" > igwn_old.db
    ```

### Initialise the `igwn_dev` instance

Log into the database container and set the global search path:

    rucio=# ALTER DATABASE rucio SET search_path = igwn_dev;
    ALTER DATABASE

Log into the the `igwn_dev` rucio server container and bootstrap the database:

    [root@rucio rucio]# tools/bootstrap.py
    Schema set in config, trying to create schema: igwn_dev
    INFO  [alembic.runtime.migration] Context impl PostgresqlImpl.
    INFO  [alembic.runtime.migration] Will assume transactional DDL.
    INFO  [alembic.runtime.migration] Running stamp_revision  -> d23453595260

Run the alembic migration:

    # cd into the directory with alembic.ini
    cd /igwn-rucio-dev/igwn_dev/etc
    # migrate: 
    alembic upgrade head

Note that in this development setup, our
`alembic.ini` is in a non-standard location, so make sure the `alembic.ini` has
the correct path to the migration scripts:

    script_location = /opt/rucio/lib/rucio/db/sqla/migrate_repo/

## Other useful actions

!!! example "Show and select postgres schemas"

    See all schemas available:

    ```sql
    SELECT schema_name FROM information_schema.schemata
    ```

    Default schema is `public`.  Set the schema to something else with e.g.

    ```sql
    rucio=# SHOW search_path;
    search_path
    -----------------
    "$user", public
    (1 row)
    rucio=# SET search_path=igwn_old;
    SET
    ```

!!! example "Destroy schema"

    Eliminate an entire schema with

    ```sql
    rucio=# DROP SCHEMA igwn_old CASCADE;
    NOTICE:  drop cascades to 158 other objects
    DETAIL:  drop cascades to type "REQUESTS_HISTORY_STATE_CHK"
    drop cascades to type "ACCOUNTS_TYPE_CHK"
    <snip>
    and 58 other objects (see server log for list)
    DROP SCHEMA
    ```
